import axios from "axios";
import { formatingTime } from './formatingTime';

// Get stories from db and mapping to humanize dates
export const getStories = async () => {
    const res = await axios.get('http://localhost:8080/api/news');
    res.data.map((story) => (
        story.created_at = formatingTime(story.created_at)
      ));
    return res.data
}
// Send delete req to api
export const deleteStory = async (id) => {
    await axios.delete(`http://localhost:8080/api/news/${id}`);
    return await getStories();
}