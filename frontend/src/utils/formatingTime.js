import { DateTime, Interval } from 'luxon';

export const formatingTime = (date) => {
    // Transform date from db
    const storyDate = DateTime.fromISO(date);
    // Set time point to tomorrow at 00
    const today = DateTime.fromObject({day: DateTime.now().toObject().day + 1 });
    // Compare today and storyDate to get a range
    const days = Math.floor(Interval.fromDateTimes(storyDate, today).length('days'))
    let timeString = '';
    // Set string to human value
    days === 0 && (timeString = storyDate.toFormat('t a'));
    days === 1 && (timeString = 'Yesterday');
    days >= 2 && (timeString = storyDate.toFormat('LLL d'));
    
    return timeString
}