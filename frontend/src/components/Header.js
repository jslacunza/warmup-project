import React, { useEffect } from 'react';
import '../styles/header.css'

export default function Header() {
  useEffect(() => {
    document.title = "HN Feed";
  }, [])

  return (
    <header>
      <div className='container'>
        <h1>HN Feed</h1>
        <span>We {'<3'} hacker news</span>
      </div>
    </header>);
}
