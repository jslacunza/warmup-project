import React, { useEffect, useState } from 'react';
import { getStories, deleteStory } from '../utils/handleStories';
import StoriesRow from './StoriesRow';

export default function StoriesApp() {
  const [stories, setStories] = useState([]);
  
  useEffect(() => {
    // Load stories and set it on the state
    async function loadStories() {
      let data = await getStories();
      setStories(data);
    }
    loadStories()
  }, []);
  // Open Story in a new tab
  const handleOpenStory= (url) => {
    // this func create an anon function to pass it to the component
    const openPage = (e) => { 
      e.stopPropagation();
      window.open(url, '_blank')
    }
    return openPage;
  }
  // Send api request to delete the news
  const handleDeleteStory =  (id) => {
    // this func create an anon function to pass it to the component
    const clickDelete = async (e) => {
      e.stopPropagation();
      setStories(await deleteStory(id));
    }
    return clickDelete;
  }
  // Maps the stories in the state and create a row for each one passing it 
  // the values and functions
  return (
    <main>
      <div className='container'>
        {stories.map((story) => (
            <StoriesRow
              key={story._id}
              id={story._id}
              title={story.title} 
              author={story.author} 
              date={story.created_at}
              url={story.url }
              handleOpenStory={handleOpenStory}
              handleDeleteStory={handleDeleteStory}
            />
        ))}
      </div >
    </main>
  );
}
