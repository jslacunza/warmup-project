import React from 'react';
import '../styles/storiesRow.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';


export default function StoriesRow({id, title, author, date, handleOpenStory, url, handleDeleteStory }) {

  return (
    <div className='story-row' onClick={handleOpenStory(url)}>
      <div className='story-title'>
        <p>
          {title}
          <span className='story-author'>- {author} -</span> 
        </p>
      </div>
      <p className='story-date'>{date}</p>
      <div onClick={handleDeleteStory(id)} className='story-delete'>
        <FontAwesomeIcon  icon={faTrash} />
      </div>
    </div>
  );
}
