import StoriesApp from './components/StoriesApp';
import Header from './components/Header';

function App() {
  // This app terurn the header and the news app
  return (
    <>
      <Header />
      <StoriesApp />
    </>
  );
}

export default App;
