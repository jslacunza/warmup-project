
# War-Up Project ·  [![coverage](https://gitlab.com/jslacunza/warmup-project/badges/main/coverage.svg)](https://https://gitlab.com/jslacunza/warmup-project) [![coverage](https://gitlab.com/jslacunza/warmup-project/badges/main/pipeline.svg)](https://https://gitlab.com/jslacunza/warmup-project)
## Hacker News

Web app based on NodeJS, NestJS, MongoDB and ReactJS that call HN API, filter and save the data into DB

## Installation

Copy this repo
```sh
git clone https://gitlab.com/jslacunza/warmup-project.git
```
Run the composer command to run the project
```sh
docker-compose up
```
Wait for the news to be saved into db
Visit http://localhost to see the app
