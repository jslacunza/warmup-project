import { Module } from '@nestjs/common';
import { StoriesModule } from 'src/stories/stories.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [StoriesModule],
  providers: [TasksService],
})
export class TasksModule {}
