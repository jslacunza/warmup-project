import { Injectable } from '@nestjs/common';
import { Interval, Timeout } from '@nestjs/schedule';
import { StoriesService } from 'src/stories/stories.service';

@Injectable()
export class TasksService {
  constructor(private storyService: StoriesService) {}
  // First fetching data
  @Timeout(1000)
  async initialFetch() {
    await this.storyService.getNewsAndSave(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
  }
  // Recurrent fetching data
  @Interval(1000 * 60 * 60) // 1s*60 => 1m*60 => 1h
  async recurrentFetch() {
    await this.storyService.getNewsAndSave(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
  }
}
