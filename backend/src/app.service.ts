import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class AppService {
  getIndex(): NotFoundException {
    throw new NotFoundException('Page not found');
  }
}
