// import { Test, TestingModule } from '@nestjs/testing';
// import { AppController } from './app.controller';
// import { AppService } from './app.service';

describe('AppController', () => {
  // let appController: AppController;

  // beforeEach(async () => {
  //   const app: TestingModule = await Test.createTestingModule({
  //     controllers: [AppController],
  //     providers: [AppService],
  //   }).compile();

  //   appController = app.get<AppController>(AppController);
  // });

  describe('root', () => {
    const t = () => {
      throw new TypeError('Page not found');
    };
    it('should return 404', () => {
      expect(t).toThrow(TypeError);
      expect(t).toThrow('Page not found');
    });
  });
});
