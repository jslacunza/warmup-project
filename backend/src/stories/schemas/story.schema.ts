// export const StorySchema = new mongoose.Schema({
//   title: String,
//   author: String,
//   created_at: String,
//   url: String,
//   is_deleted: Boolean,
//   api_id: String,
// });
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type StoryDocument = Story & Document;

@Schema()
export class Story {
  @Prop()
  title: string;
  @Prop()
  author: string;
  @Prop()
  created_at: string;
  @Prop()
  url: string;
  @Prop()
  is_deleted: boolean;
  @Prop()
  api_id: string;
}

export const StorySchema = SchemaFactory.createForClass(Story);
