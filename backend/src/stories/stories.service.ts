import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import axios from 'axios';
import { Model } from 'mongoose';
import { Story } from './interfaces/story.interface';

@Injectable()
export class StoriesService {
  constructor(@InjectModel('Story') private readonly newStory: Model<Story>) {}
  private readonly logger = new Logger(StoriesService.name);

  // show available news
  async findAll(): Promise<Story[]> {
    return await this.newStory
      .find({ is_deleted: false })
      .sort({ created_at: -1 });
  }

  async checkExist(id: string): Promise<boolean> {
    const story = await this.newStory.findOne({ api_id: id });
    return story ? true : false;
  }

  async createStory(story: Story): Promise<Story> {
    return await this.newStory.create(story);
  }

  async hideStory(id: string): Promise<string> {
    await this.newStory.findByIdAndUpdate(id, { is_deleted: true });
    return 'Story Deleted';
  }

  // Getting data and filtering it
  async getNewsAndSave(url: string): Promise<any[]> {
    // Get the response
    const res = await axios.get(url);
    const news = await Promise.all(
      res.data.hits.map(async (story) => {
        // Add title or story_title to title
        const title: string = story.title || story.story_title;
        // Add url or story_url to url
        const url: string = story.url || story.story_url;
        // check if it is in DB
        const exist = await this.checkExist(story.objectID);
        // filter news by missing title or url
        if (title && url && !exist) {
          const newStory: Story = {
            title: title,
            author: story.author,
            created_at: story.created_at,
            url: url,
            is_deleted: false,
            api_id: story.objectID,
          };
          // Save filtered news into DB
          await this.createStory(newStory);
          return newStory;
        }
      }),
    );
    const uploadedNews = news.filter(Boolean);
    this.logger.log(`${uploadedNews.length} stories stored into database`);
    return uploadedNews;
  }
}
