import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import axios from 'axios';
import { Model } from 'mongoose';
import { axiosRes, dataDB } from '../../test/fetchData';
import { Story } from './schemas/story.schema';
import { StoriesService } from './stories.service';
// Story to upload
const storyBase: Story = {
  title: 'Node-Red',
  author: 'knolleary',
  created_at: '2022-01-25T12:31:28.000Z',
  url: 'https://github.com/naimo84/awesome-nodered',
  is_deleted: false,
  api_id: '30071056',
};
// Story response from DB
const storyDB = {
  title: 'Node-Red',
  author: 'knolleary',
  created_at: '2022-01-25T12:31:28.000Z',
  url: 'https://github.com/naimo84/awesome-nodered',
  is_deleted: false,
  api_id: '30071056',
  _id: '61f13acdd0029df5637d0c53',
  __v: 0,
};

// mocking injected model in stories services
// this are all fn called inside services
const mockStoryModel = {
  find: jest.fn(),
  sort: jest.fn(),
  findOne: jest.fn((obj) => {
    return obj.api_id === storyDB.api_id ? storyDB : null;
  }),
  create: jest.fn((obj) => {
    return obj ? storyDB : null;
  }),
  findByIdAndUpdate: jest.fn((id: string) => {
    return id === storyDB._id ? 'Story Deleted' : null;
  }),
};

jest.mock('axios');

describe('StoriesService', () => {
  let service: StoriesService;
  let model: Model<Story>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StoriesService,
        {
          provide: getModelToken('Story'),
          useValue: mockStoryModel,
        },
      ],
    }).compile();

    service = module.get<StoriesService>(StoriesService);
    model = module.get<Model<Story>>(getModelToken('Story'));
  });

  describe('findAll()', () => {
    test('should return arr stories from db', async () => {
      jest.spyOn(model, 'find').mockReturnValue({
        sort: jest.fn().mockReturnValue([storyDB]),
      } as any);
      expect(await service.findAll()).toEqual([storyDB]);
    });
  });

  describe('checkExist()', () => {
    test('should return true', async () => {
      expect(await service.checkExist('30071056')).toEqual(true);
    });
    test('should return false', async () => {
      expect(await service.checkExist('')).toEqual(false);
    });
  });

  describe('createStory()', () => {
    test('should return story from db', async () => {
      expect(await service.createStory(storyBase)).toEqual(storyDB);
    });
  });

  describe('hideStory()', () => {
    test('should should return StoryDeleted', async () => {
      expect(await service.hideStory('61f13acdd0029df5637d0c53')).toEqual(
        'Story Deleted',
      );
    });
  });
  describe('getNewsAndSave', () => {
    const mockedAxios = axios as jest.Mocked<typeof axios>;
    test('should return arr of stories uploaded to db', async () => {
      mockedAxios.get.mockImplementation(() => Promise.resolve(axiosRes));
      expect(await service.getNewsAndSave('')).toEqual(dataDB);
    });
  });
});
