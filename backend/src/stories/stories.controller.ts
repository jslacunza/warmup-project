import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
} from '@nestjs/common';
import { StoryDto } from './dto/story.dto';
import { Story } from './interfaces/story.interface';
import { StoriesService } from './stories.service';

@Controller('api/news')
export class StoriesController {
  constructor(private readonly storiesService: StoriesService) {}

  // create new
  @Post()
  createNew(@Body() createStoryDto: StoryDto): Promise<Story> {
    return this.storiesService.createStory(createStoryDto);
  }

  // read news
  @Get()
  findAll(): Promise<Story[]> {
    return this.storiesService.findAll();
  }

  // delete new
  @Delete(':id')
  @HttpCode(200)
  deleteNew(@Param('id') id): Promise<string> {
    return this.storiesService.hideStory(id);
  }
}
