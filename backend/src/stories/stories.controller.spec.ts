import { Test } from '@nestjs/testing';
import { StoriesController } from './stories.controller';
import { StoriesService } from './stories.service';

const storyBase = {
  title: 'Node-Red',
  author: 'knolleary',
  created_at: '2022-01-25T12:31:28.000Z',
  url: 'https://github.com/naimo84/awesome-nodered',
  is_deleted: false,
  api_id: '30071056',
};

describe('StoriesController', () => {
  let controller: StoriesController;

  const mockStoriesService = {
    // this should have any fn called on the controller
    createStory: jest.fn((story) => {
      return {
        _id: Date.now(),
        __v: Date.now(),
        ...story,
      };
    }),
    findAll: jest.fn(() => {
      return [
        {
          _id: Date.now(),
          __v: Date.now(),
          ...storyBase,
        },
      ];
    }),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [StoriesController],
      providers: [StoriesService],
    })
      .overrideProvider(StoriesService)
      .useValue(mockStoriesService)
      .compile();

    controller = moduleRef.get<StoriesController>(StoriesController);
  });

  it('should return created story', () => {
    // call createNew from the controller
    expect(controller.createNew(storyBase)).toEqual({
      _id: expect.any(Number),
      __v: expect.any(Number),
      ...storyBase,
    });
  });

  it('should return arr story', () => {
    // call findAll from the controller
    expect(controller.findAll()).toEqual([
      {
        _id: expect.any(Number),
        __v: expect.any(Number),
        ...storyBase,
      },
    ]);
  });
});
