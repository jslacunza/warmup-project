export interface Story {
  title: string;
  author: string;
  created_at: string;
  url: string;
  is_deleted: boolean;
  api_id: string;
}
