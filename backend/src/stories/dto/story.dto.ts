export class StoryDto {
  readonly title: string;
  readonly author: string;
  readonly created_at: string;
  readonly url: string;
  readonly is_deleted: boolean;
  readonly api_id: string;
}
