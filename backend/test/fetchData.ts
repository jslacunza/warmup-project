export const axiosRes = {
  data: {
    hits: [
      {
        created_at: '2022-01-20T11:21:15.000Z',
        title: null,
        url: null,
        author: '0x008',
        points: null,
        story_text: null,
        comment_text:
          'Is binary size a concern to you (probably not if you come from go)? If not: don&#x27;t worry about it. You can compile everything into a binary when you embed the runtime. Ever thought about JVM-languages?<p>Do you need static typing, multithreading and fast execution? Maybe try Scala or Clojure.<p>If functional programming is not your main concern, but you want a mature ecosystem with one-liner http server and medium-fast execution? Maybe try python or nodejs.',
        num_comments: null,
        story_id: 29971945,
        story_title:
          'Ask HN: Is there a functional programming language with the benefits of Go?',
        story_url: null,
        parent_id: 29971945,
        created_at_i: 1642677675,
        _tags: ['comment', 'author_0x008', 'story_29971945'],
        objectID: '30007481',
        _highlightResult: {
          author: {
            value: '0x008',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "Is binary size a concern to you (probably not if you come from go)? If not: don't worry about it. You can compile everything into a binary when you embed the runtime. Ever thought about JVM-languages?<p>Do you need static typing, multithreading and fast execution? Maybe try Scala or Clojure.<p>If functional programming is not your main concern, but you want a mature ecosystem with one-liner http server and medium-fast execution? Maybe try python or <em>nodejs</em>.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Ask HN: Is there a functional programming language with the benefits of Go?',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-20T05:36:01.000Z',
        title: null,
        url: null,
        author: 'chrischen',
        points: null,
        story_text: null,
        comment_text:
          'All the companies using Rescript (formerly ReasonML + Bucklescript compiler) and ones using Reasonml right now are effectively using Ocaml. It allows you to run Ocaml but target web browsers or Nodejs.<p>This includes apps like TinyMCE, ahrefs, and around 50% of messenger.com code [1].<p>[1] <a href="https:&#x2F;&#x2F;reasonml.github.io&#x2F;blog&#x2F;2017&#x2F;09&#x2F;08&#x2F;messenger-50-reason" rel="nofollow">https:&#x2F;&#x2F;reasonml.github.io&#x2F;blog&#x2F;2017&#x2F;09&#x2F;08&#x2F;messenger-50-reas...</a>',
        num_comments: null,
        story_id: 29998957,
        story_title:
          'Hotcaml: An OCaml interpreter with watching and reloading',
        story_url: 'https://github.com/let-def/hotcaml',
        parent_id: 30001688,
        created_at_i: 1642656961,
        _tags: ['comment', 'author_chrischen', 'story_29998957'],
        objectID: '30005126',
        _highlightResult: {
          author: {
            value: 'chrischen',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'All the companies using Rescript (formerly ReasonML + Bucklescript compiler) and ones using Reasonml right now are effectively using Ocaml. It allows you to run Ocaml but target web browsers or <em>Nodejs</em>.<p>This includes apps like TinyMCE, ahrefs, and around 50% of messenger.com code [1].<p>[1] <a href="https://reasonml.github.io/blog/2017/09/08/messenger-50-reason" rel="nofollow">https://reasonml.github.io/blog/2017/09/08/messenger-50-reas...</a>',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Hotcaml: An OCaml interpreter with watching and reloading',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://github.com/let-def/hotcaml',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-20T00:04:03.000Z',
        title:
          'Ask HN: Existing framework to build/control a UI from a workflow?',
        url: null,
        author: 'bbatchelder',
        points: 1,
        story_text:
          'Before I build something myself, I wanted to check if there is anything already out there that does this.<p>Essentially want to build custom apps by designing them in a visual workflow builder to define the interactivity.<p>Imagine being able to drag a &quot;Prompt&quot; activity, define the content you want to display to the user, and the options for how the user can respond (ex: show Yes&#x2F;No buttons, or other choices), and then have a decision activity that can evaluate what the user chose and branch to some other UI - like another prompt dialog, or a file upload dialog, or a form, etc etc.<p>The more activities that you implement the richer the app you could build.<p>Right now I am leaning towards using &quot;node red&quot; as the workflow engine, and building a bunch of custom nodes that represent all the UI functionality I want - and then implementing an app host in Angular or React that will integrate with the workflow - probably via web socket messages.<p>So at the end of the day, this App Host UI is receiving commands from the workflow as far as what content to display to the user, and then sending back what the user is doing with the UI to the workflow so it can send follow up commands.',
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1642637043,
        _tags: ['story', 'author_bbatchelder', 'story_30002330', 'ask_hn'],
        objectID: '30002330',
        _highlightResult: {
          title: {
            value:
              'Ask HN: Existing framework to build/control a UI from a workflow?',
            matchLevel: 'none',
            matchedWords: [],
          },
          author: {
            value: 'bbatchelder',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_text: {
            value:
              'Before I build something myself, I wanted to check if there is anything already out there that does this.<p>Essentially want to build custom apps by designing them in a visual workflow builder to define the interactivity.<p>Imagine being able to drag a &quot;Prompt&quot; activity, define the content you want to display to the user, and the options for how the user can respond (ex: show Yes/No buttons, or other choices), and then have a decision activity that can evaluate what the user chose and branch to some other UI - like another prompt dialog, or a file upload dialog, or a form, etc etc.<p>The more activities that you implement the richer the app you could build.<p>Right now I am leaning towards using &quot;node red&quot; as the workflow engine, and building a bunch of custom <em>nodes</em> that represent all the UI functionality I want - and then implementing an app host in Angular or React that will integrate with the workflow - probably via web socket messages.<p>So at the end of the day, this App Host UI is receiving commands from the workflow as far as what content to display to the user, and then sending back what the user is doing with the UI to the workflow so it can send follow up commands.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
        },
      },
    ],
    nbHits: 22042,
    page: 0,
    nbPages: 50,
    hitsPerPage: 20,
    exhaustiveNbHits: true,
    exhaustiveTypo: true,
    query: 'nodejs',
    params:
      'advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs',
    renderingContent: {},
    processingTimeMS: 9,
  },
};

export const dataDB = [
  {
    title: 'Hotcaml: An OCaml interpreter with watching and reloading',
    author: 'chrischen',
    created_at: '2022-01-20T05:36:01.000Z',
    url: 'https://github.com/let-def/hotcaml',
    is_deleted: false,
    api_id: '30005126',
  },
];
